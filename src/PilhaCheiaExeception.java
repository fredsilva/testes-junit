
public class PilhaCheiaExeception extends RuntimeException {

	public PilhaCheiaExeception(String message) {
		super(message);		
	}
}
